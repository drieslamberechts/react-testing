import { useState, createContext, useContext, useReducer } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import {OrderPageProvider, OrderContext} from './OrderContext.tsx'


function Patient({patient}): JSX.Element {
  return (
    <div>
      <h4>Patient</h4>
      <div>First Name: {patient.firstName}</div>
      <div>Last Name: {patient.lastName}</div>
    </div>
  );
}

function Procedures({procedures}): JSX.Element {
  return (
    <div>
      <h4>Procedures</h4>
      {procedures.map((x,i) => <div key={i}>Procedure</div>)}
    </div>);
}

function Checks({checks}): JSX.Element {
  return (
    <div>
      <h4>Checks</h4>
      {checks.map((x, i) => <div key={i}>Check</div>)}
    </div>
  );
}

function OrderPage() {
  const {state, dispatch} = useContext(OrderContext);
  
  return (
      <div>
        <h1>Order Detail</h1>
        <Patient patient={state.patient} />
        <hr />
        <Procedures procedures={state.procedures} />
        <hr />
        <Checks checks={state.checks} />
        <button onClick={e => dispatch({action: 'addProcedure'})}>Add Procedure</button>
        <button onClick={e => dispatch({action: 'addCheck'})}>Add Check</button>
      </div>
  );
}

function App() {
  return (
    <>
      <OrderPageProvider>
        <OrderPage />
      </OrderPageProvider>
    </>
  )
}

export default App
