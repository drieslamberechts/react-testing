class Order {
  constructor() {
    this.patient = {
      firstName: 'Dries',
      lastName: 'Lamberechts'
    };
    this.procedures = [];
    this.checks = [];
  }
}

export { Order };
