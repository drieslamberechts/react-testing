import { createContext, useReducer, useMemo } from 'react';
import { Order } from './Order.ts'

// NOTE: React Docs: "The defaultValue argument is only used when a component does not have a matching Provider above it in the tree. This can be helpful for testing components in isolation without wrapping them."
const OrderContext = createContext();

function OrderPageProvider({children}) {
  const orderReducer = (order, data) => {
    switch (data.action) {
      case 'addProcedure':
        return {...order, procedures: [...order.procedures, {}]}; // data could have any information we need to add to the model
      case 'addCheck':
        return {...order, checks: [...order.checks, {}]};
      default:
        console.error('Unknown action');
    }
  };
  const [order, dispatch] = useReducer(orderReducer, new Order());

  return (
    <OrderContext.Provider value={useMemo(() => ({state: order, dispatch}), [order])}>
      {children}
    </OrderContext.Provider>
  );
}

export { OrderPageProvider, OrderContext };
